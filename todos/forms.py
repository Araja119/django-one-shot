from django import forms
from todos.models import TodoList
from django.forms import ModelForm

class ListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]

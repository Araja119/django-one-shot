from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import ListForm

def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    # This is called todo_list because you're pulling a specific list
    todo_list = get_object_or_404(TodoList, id=id)
    # This is called items because you want to get multiple items from within that list
    items = todo_list.items.all()
    context = {
        "todo_list": todo_list,
        "items": items
    }
    return render(request, "todos/detail.html", context)

def create_todo_list(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = ListForm()
    context = {
        "form": form
    }

    return render(request, "todos/create.html", context)
